package vondracek.uhk.soundcontrol;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import vondracek.uhk.soundcontrol.data.PositionData;
import vondracek.uhk.soundcontrol.data.PositionDataShow;
import vondracek.uhk.soundcontrol.service.DatabaseService;
import vondracek.uhk.soundcontrol.service.PositionService;
import vondracek.uhk.soundcontrol.service.Utils;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    @Test
    public void dbTest1() {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        DatabaseService databaseService = new DatabaseService(appContext);

        PositionData positionData = new PositionData();
        positionData.setLatitude(55.54F);
        positionData.setLongtitude(14.54F);
        positionData.setName("Home");
        positionData.setRadius(100);
        positionData.setTimeFrom("14:00");
        positionData.setTimeTo("15:00");
        positionData.setAddress("Praha");
        positionData.setDaily(false);
        positionData.setMonday(true);
        positionData.setTuesday(true);
        positionData.setWednesday(true);
        positionData.setThursday(true);
        positionData.setFriday(false);
        positionData.setSaturday(false);
        positionData.setSunday(false);

        databaseService.insertData(positionData);

        PositionData data = databaseService.getData(1);
        assertEquals("Home", data.getName());

        List<PositionDataShow> positionDataShows = databaseService.readAll();
        PositionDataShow positionDataShow = positionDataShows.get(0);
        assertEquals("Home", positionDataShow.getName());

    }

    @Test
    public void test2(){
        boolean positionInCircle = PositionService.isPositionInCircle(49.971872, 16.391659,
                500, 49.972965, 16.391019);
        assertTrue(positionInCircle);

        positionInCircle = PositionService.isPositionInCircle(49.971872, 16.391659,
                500, 49.867786, 16.308484);
        assertFalse(positionInCircle);
    }

    @Test
    public void testTime(){
        Integer hours = Utils.getHours();
        System.out.println(hours);

        Integer minutes = Utils.getMinutes();
        System.out.println(minutes);

        String time = Utils.getTime();
        System.out.println(time);

        boolean timeIn = Utils.inTime("21:33", "22:15");
        System.out.println(timeIn);

    }
}
