package vondracek.uhk.soundcontrol.service;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import vondracek.uhk.soundcontrol.R;

public class PositionService implements LocationListener {

    private Context context;
    private PositionChangeListener positionChangeListener;
    private LocationManager locationManager;
    private Location location = null;

    public Location getLocation(){
        return location;
    }

    public PositionService(Context appCompatActivity, PositionChangeListener positionChangeListener) {
        this.context = appCompatActivity;
        this.positionChangeListener = positionChangeListener;
        locationManager = (LocationManager)
                appCompatActivity.getSystemService(Context.LOCATION_SERVICE);
    }

    public void searchPosition() {
        if (location != null) {
            onLocationChanged(location);
        } else {

            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }

            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, 5000, 10, this);

        }
    }

    @Override
    public void onLocationChanged(Location loc) {
        location = loc;
        positionChangeListener.finished(loc);
    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(context, context.getString(R.string.onProviderDisabledString), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @FunctionalInterface
    public interface PositionChangeListener {
        void finished(Location loc);
    }

    public static boolean isPositionInCircle(double circleLat, double circleLong,
                                             double circleRadius, double locationLat,
                                             double locationLong) {
        float[] distance = new float[2];
        Location.distanceBetween(locationLat, locationLong,
                circleLat, circleLong, distance);
        return !(distance[0] > circleRadius);
    }

    private static final String TAG = "GeocodingLocation";

    public static LatLng getAddressFromLocation(final String locationAddress,
                                                final Context context) {

        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List addressList = geocoder.getFromLocationName(locationAddress, 1);
            if (addressList != null && addressList.size() > 0) {
                Address address = (Address) addressList.get(0);
                return new LatLng(address.getLatitude(), address.getLongitude());
            }
            return null;
        } catch (IOException e) {
            Log.e(TAG, "Unable to connect to Geocoder", e);
            return null;
        }
    }
}

