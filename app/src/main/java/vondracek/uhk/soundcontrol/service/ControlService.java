package vondracek.uhk.soundcontrol.service;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.media.AudioManager;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Looper;
import android.widget.Switch;
import android.widget.Toast;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import vondracek.uhk.soundcontrol.R;
import vondracek.uhk.soundcontrol.data.PositionData;

public class ControlService extends IntentService {

    private PositionService positionService;
    private Location location = null;
    private Context context;
    private DatabaseService databaseService;
    private LocalDateTime lastNotification;

    private AppCompatActivity appCompatActivity;


    public ControlService() {
        super("ControlService");
    }

    @Override
    protected void onHandleIntent(Intent workIntent) {

        System.out.println("start");

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {

                                      @Override
                                      public void run() {
                                              check();
                                      }
                                  },
                5000, 5000);
    }

    private void setSoundMode(PositionData plan) {
        AudioManager myAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        BluetoothAdapter bAdapter = BluetoothAdapter.getDefaultAdapter();

        int mode = plan.getMode();
        switch (mode) {
            case 0:
                System.out.println("set vibrate mode");
                myAudioManager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
                break;
            case 1:
                System.out.println("set normal mode");
                myAudioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                break;
            case 2:
                System.out.println("set wifi on");
                wifiManager.setWifiEnabled(true);
                break;
            case 3:
                System.out.println("set wifi off");
                wifiManager.setWifiEnabled(false);
                break;
            case 4:
                System.out.println("set bluetooth on");
                bAdapter.enable();
                break;
            case 5:
                System.out.println("set bluetooth off");
                bAdapter.disable();
                break;
        }
    }


    private PositionData check() {
        List<PositionData> positionDataList = databaseService.readAllInfo();
        PositionData plan = null;

        for (int i = 0; i < positionDataList.size(); i++) {
            if (chech(positionDataList.get(i))) {
                setSoundMode(positionDataList.get(i));
            }
        }
        return plan;
    }

    private boolean chech(PositionData positionData) {
        Boolean todayInScheme = Utils.isTodayInScheme(positionData);
        Boolean timeIn = Utils.inTime(positionData.getTimeFrom(), positionData.getTimeTo());
        Boolean inPosition = false;
        if (location != null) {
            inPosition = PositionService.isPositionInCircle(positionData.getLatitude(),
                    positionData.getLongtitude(), positionData.getRadius(), location.getLatitude(),
                    location.getLongitude());
        }else{
            if(todayInScheme && timeIn){
                showNotification("Není povoleno zjišťování polohy", "Je naplánována změna nastavení " + getSettingName(positionData.getMode()) + ", ale není" +
                        " informace o poloze.");
                return false;
            }else{
                return false;
            }
        }

        if (todayInScheme && timeIn && inPosition) {
            return true;
        } else {
            return false;
        }
    }

    private String getSettingName(Integer mode){
        switch(mode){
            case 0: return "zvuku";
            case 1: return "zvuku";
            case 2: return "wifi";
            case 3: return "wifi";
            case 4: return "bluetooth";
            case 5: return "bluetooth";
            default: return "";
        }
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        context = this;
        databaseService = new DatabaseService(this);
        Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();

        positionService = new PositionService(context, new PositionService.PositionChangeListener() {
            @Override
            public void finished(Location loc) {
                location = loc;
                Toast.makeText(context, "position changed " + loc.getLongitude() + " " + loc.getLatitude(), Toast.LENGTH_SHORT).show();

            }
        });

        if(positionService.getLocation() != null){
            location = positionService.getLocation();
        }

        positionService.searchPosition();

        if(positionService.getLocation() != null){
            location = positionService.getLocation();
        }

        return super.onStartCommand(intent, flags, startId);
    }

    private void showNotification(String title, String text){
        LocalDateTime now = LocalDateTime.now();

        if(databaseService.getDataNastaveni(1) == 0){
            return;
        }

        if(lastNotification == null || now.minusMinutes(1).isAfter(lastNotification)) {

            int notifyID = 1;
            String CHANNEL_ID = "my_channel_01";
            CharSequence name = "name";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);

            Notification notification2 =
                    new NotificationCompat.Builder(context)
                            .setSmallIcon(R.drawable.icon)
                            .setContentTitle(title)
                            .setContentText(text)
                            .setChannelId(CHANNEL_ID).build();

            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.createNotificationChannel(mChannel);
            mNotificationManager.notify(notifyID, notification2);

            lastNotification = LocalDateTime.now();
        }
    }

}
