package vondracek.uhk.soundcontrol.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import vondracek.uhk.soundcontrol.data.PositionData;

public class Utils {

    public static String getTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        return sdf.format(new Date());
    }

    public static Integer getHours() {

        String hours = new SimpleDateFormat("HH").format(new Date());
        if (hours.charAt(0) == '0') {
            return Integer.valueOf(hours.charAt(1));
        } else {
            return Integer.valueOf(hours);
        }
    }

    public static Integer getMinutes() {
        String minutes = new SimpleDateFormat("mm").format(new Date());
        if (minutes.charAt(0) == '0') {
            return Integer.valueOf(minutes.charAt(1));
        } else {
            return Integer.valueOf(minutes);
        }
    }

    public static Integer getDayOfWeek() {
        return Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
    }

    public static Boolean inTime(String timeFrom, String timeTo) {
        Integer time = removeColonFromString(getTime());
        Integer from = removeColonFromString(timeFrom);
        Integer to = removeColonFromString(timeTo);

        return numberBetween(time, from, to);

    }

    private static Integer removeColonFromString(String time){
        return Integer.valueOf(time.replace(":", ""));
    }

    private static Boolean numberBetween(Integer number, Integer from, Integer to){
        return number >= from && number <= to ? true : false;
    }

    private static Integer getHoursFromString(String time) {
        if (time.charAt(0) == '0') {
            return Integer.valueOf(time.charAt(1));
        } else {
            Integer minutes = time.charAt(1) + time.charAt(2);
            return Integer.valueOf(minutes);
        }
    }

    private static Integer getMinutesFromString(String time) {
        if (time.charAt(3) == '0') {
            return Integer.valueOf(time.charAt(4));
        } else {
            Integer minutes = time.charAt(3) + time.charAt(4);
            return Integer.valueOf(minutes);
        }
    }

    public static Boolean isTodayInScheme(PositionData positionData) {

        if (positionData.getDaily()) {
            return true;
        }

        switch (getDayOfWeek()) {
            case Calendar.MONDAY:
                return positionData.getMonday();
            case Calendar.TUESDAY:
                return positionData.getTuesday();
            case Calendar.WEDNESDAY:
                return positionData.getWednesday();
            case Calendar.THURSDAY:
                return positionData.getThursday();
            case Calendar.FRIDAY:
                return positionData.getFriday();
            case Calendar.SATURDAY:
                return positionData.getSaturday();
            case Calendar.SUNDAY:
                return positionData.getSunday();
            default:
                return false;
        }
    }

    public static String soundModeToString(int mode){
        switch(mode){
            case 0: return "Vibrate mode";
            case 1: return "Normal mode";
            case 2: return "Wifi on";
            case 3: return "Wifi off";
            case 4: return "Bluetooth on";
            case 5: return "Bluetooth off";
            default: return null;
        }
    }

}
