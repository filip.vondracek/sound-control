package vondracek.uhk.soundcontrol.service;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import vondracek.uhk.soundcontrol.data.PositionData;
import vondracek.uhk.soundcontrol.data.PositionDataShow;

public class DatabaseService extends SQLiteOpenHelper {

    private static String DB_NAME = "database1.db";
    private static int DB_VERSION = 1;
    private static String TABLE_NAME = "table2";
    private static String TABLE_NAME_NASTAVENI = "nastaveni";
    private static String CREATE_TABLE
            = "create table table2(id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "latitude REAL, longitude REAL, name TEXT, " +
            "radius INTEGER, timeFrom TEXT, timeTo TEXT, " +
            "address TEXT, daily INTEGER, " +
            "monday INTEGER, tuesday INTEGER, wednesday INTEGER, " +
            "thursday INTEGER, friday INTEGER, saturday INTEGER, " +
            "sunday INTEGER, mode INTEGER);";

    private static String CREATE_TABLE_NASTAVENI
            = "create table nastaveni(id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "nastaveni INTEGER);";

    public DatabaseService(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
        db.execSQL(CREATE_TABLE_NASTAVENI);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table table2");
        db.execSQL("drop table nastaveni");
        onCreate(db);
    }

    public PositionData getData(Integer id) {
        PositionData position = new PositionData();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cs = db.query(TABLE_NAME, new String[]{"id", "latitude", "longitude", "name", "radius",
                        "timeFrom", "timeTo", "address", "daily", "monday", "tuesday", "wednesday", "thursday",
                        "friday", "saturday", "sunday", "mode"},
                "id = ?", new String[]{id + ""}, null, null, null, null);

        while (cs.moveToNext()) {
            position.setLatitude(cs.getFloat(1));
            position.setLongtitude(cs.getFloat(2));
            position.setName(cs.getString(3));
            position.setRadius(cs.getInt(4));
            position.setTimeFrom(cs.getString(5));
            position.setTimeTo(cs.getString(6));
            position.setAddress(cs.getString(7));
            position.setDaily(intToBoolean(cs.getInt(8)));
            position.setMonday(intToBoolean(cs.getInt(9)));
            position.setTuesday(intToBoolean(cs.getInt(10)));
            position.setWednesday(intToBoolean(cs.getInt(11)));
            position.setThursday(intToBoolean(cs.getInt(12)));
            position.setFriday(intToBoolean(cs.getInt(13)));
            position.setSaturday(intToBoolean(cs.getInt(14)));
            position.setSunday(intToBoolean(cs.getInt(15)));
            position.setMode(cs.getInt(16));

        }

        return position;

    }

    private Boolean intToBoolean(int i) {
        return i == 0 ? false : true;
    }

    private int booleanToInt(Boolean b) {
        return b == true ? 1 : 0;
    }

    public void deleteData() {
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL("drop table table2");
        db.execSQL(CREATE_TABLE);
    }

    public void deleteData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL("delete from " + TABLE_NAME + " where id='" + id + "'");
    }

    public void insertData(PositionData data) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues mValues = new ContentValues();
        mValues.put("latitude", data.getLatitude());
        mValues.put("longitude", data.getLongtitude());
        mValues.put("name", data.getName());
        mValues.put("radius", data.getRadius());
        mValues.put("timeFrom", data.getTimeFrom());
        mValues.put("timeTo", data.getTimeTo());
        mValues.put("address", data.getAddress());
        mValues.put("daily", booleanToInt(data.getDaily()));
        mValues.put("monday", booleanToInt(data.getMonday()));
        mValues.put("tuesday", booleanToInt(data.getTuesday()));
        mValues.put("wednesday", booleanToInt(data.getWednesday()));
        mValues.put("thursday", booleanToInt(data.getThursday()));
        mValues.put("friday", booleanToInt(data.getFriday()));
        mValues.put("saturday", booleanToInt(data.getSaturday()));
        mValues.put("sunday", booleanToInt(data.getSunday()));
        mValues.put("mode", data.getMode());

        db.insert(TABLE_NAME, null, mValues);
    }


    public List<PositionData> readAllInfo() {
        SQLiteDatabase db = this.getReadableDatabase();
        List<PositionData> result = new ArrayList<>();
        Cursor cs = db.query(TABLE_NAME, new String[]{"id", "name", "address",
                        "latitude", "longitude", "radius", "timeFrom", "timeTo, " +
                        "daily", "monday", "tuesday", "wednesday", "thursday", "friday",
                        "saturday", "sunday", "mode"},
                null, null, null, null, "id DESC");

        while (cs.moveToNext()) {
            PositionData tmpData = new PositionData();
            tmpData.setId(cs.getInt(0));
            tmpData.setName(cs.getString(1));
            tmpData.setAddress(cs.getString(2));
            tmpData.setLatitude(cs.getFloat(3));
            tmpData.setLongtitude(cs.getFloat(4));
            tmpData.setRadius(cs.getInt(5));
            tmpData.setTimeFrom(cs.getString(6));
            tmpData.setTimeTo(cs.getString(7));
            tmpData.setDaily(intToBoolean(cs.getInt(8)));
            tmpData.setMonday(intToBoolean(cs.getInt(9)));
            tmpData.setTuesday(intToBoolean(cs.getInt(10)));
            tmpData.setWednesday(intToBoolean(cs.getInt(11)));
            tmpData.setThursday(intToBoolean(cs.getInt(12)));
            tmpData.setFriday(intToBoolean(cs.getInt(13)));
            tmpData.setSaturday(intToBoolean(cs.getInt(14)));
            tmpData.setSunday(intToBoolean(cs.getInt(15)));
            tmpData.setMode(cs.getInt(16));
            result.add(tmpData);
        }
        return result;
    }

    public ArrayList<PositionDataShow> readAll() {

        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<PositionDataShow> result = new ArrayList<>();
        Cursor cs = db.query(TABLE_NAME, new String[]{"id", "name", "address", "mode"},
                null, null, null, null, "id DESC");

        while (cs.moveToNext()) {
            PositionDataShow tmpData = new PositionDataShow();
            tmpData.setId(cs.getInt(0));
            tmpData.setName(cs.getString(1));
            tmpData.setAddress(cs.getString(2));
            tmpData.setMode(cs.getInt(3));
            result.add(tmpData);
        }
        return result;
    }

    //NASTAVENI

    public Integer getDataNastaveni(Integer id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cs = db.query(TABLE_NAME_NASTAVENI, new String[]{"id", "nastaveni"},
                "id = ?", new String[]{id + ""}, null, null, null, null);

        while (cs.moveToNext()) {
            return cs.getInt(1);
        }
        return -1;
    }

    public void deleteDataNastaveni(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL("delete from " + TABLE_NAME_NASTAVENI + " where id='" + id + "'");
    }

    public void insertDataNastaveni(Integer id, Integer nastaveni) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues mValues = new ContentValues();
        mValues.put("id", id);
        mValues.put("nastaveni", nastaveni);

        db.insert(TABLE_NAME_NASTAVENI, null, mValues);
    }
}
