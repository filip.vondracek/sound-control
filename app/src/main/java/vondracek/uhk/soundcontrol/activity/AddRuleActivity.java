package vondracek.uhk.soundcontrol.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import vondracek.uhk.soundcontrol.data.PositionData;
import vondracek.uhk.soundcontrol.R;
import vondracek.uhk.soundcontrol.service.DatabaseService;
import vondracek.uhk.soundcontrol.service.PositionService;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class AddRuleActivity extends AppCompatActivity implements OnMapReadyCallback {
    private MapView mapView;
    private GoogleMap mMap;

    private static final String MAP_VIEW_BUNDLE_KEY = "AIzaSyBDFzsnEfeuplk7NK4XDpDfq5Bl4bZCJOk";

    private Circle circle;
    private Marker marker;

    Integer id = null;

    DatabaseService databaseService = new DatabaseService(this);

    LatLng position = null;

    TextView textViewName;
    TextView textViewAddress;
    TextView textViewRadius;
    TextView textViewTimeFrom;
    TextView textViewTimeTo;

    CheckBox checkBoxDaily;
    CheckBox checkBoxMonday;
    CheckBox checkBoxThuesday;
    CheckBox checkBoxWednesday;
    CheckBox checkBoxThursday;
    CheckBox checkBoxFriday;
    CheckBox checkBoxSaturday;
    CheckBox checkBoxSunday;

    Button buttonDelete;
    Button buttonSave;
    Spinner spinnerMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_rule);

        Bundle bu = getIntent().getExtras();
        if (bu != null) {
            String idString = bu.getString("id");
            if (idString != null) {
                id = Integer.valueOf(idString);
            }
        }

        textViewAddress = findViewById(R.id.editTextAddress);
        textViewName = findViewById(R.id.editTextName);
        textViewRadius = findViewById(R.id.editTextRadius);
        textViewTimeFrom = findViewById(R.id.editTextTimeForm);
        textViewTimeTo = findViewById(R.id.editTextTImeTo);

        checkBoxDaily = findViewById(R.id.checkBoxDaily);
        checkBoxMonday = findViewById(R.id.checkBoxMonday);
        checkBoxThuesday = findViewById(R.id.checkBoxTuesday);
        checkBoxWednesday = findViewById(R.id.checkBoxWednesday);
        checkBoxThursday = findViewById(R.id.checkBoxThursday);
        checkBoxFriday = findViewById(R.id.checkBoxFriday);
        checkBoxSaturday = findViewById(R.id.checkBoxSaturday);
        checkBoxSunday = findViewById(R.id.checkBoxSunday);

        buttonDelete = findViewById(R.id.buttonDelete);
        buttonSave = findViewById(R.id.buttonSave);

        //spinner
        spinnerMode = findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.sound_mode, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMode.setAdapter(adapter);

        if (id != null) {
            loadData(databaseService.getData(id));
        }

        if (id == null) {
            buttonDelete.setEnabled(false);
        }

        textViewRadius.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                //if (!hasFocus) {
                    updateMapView();
                //}
            }
        });

        textViewAddress.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                //if (!hasFocus) {
                    updateMapView();
               // }
            }
        });

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete();
                finish();
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
                finish();
            }
        });

        checkBoxDaily.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkBoxMonday.setEnabled(!checkBoxDaily.isChecked());
                checkBoxThuesday.setEnabled(!checkBoxDaily.isChecked());
                checkBoxWednesday.setEnabled(!checkBoxDaily.isChecked());
                checkBoxThursday.setEnabled(!checkBoxDaily.isChecked());
                checkBoxFriday.setEnabled(!checkBoxDaily.isChecked());
                checkBoxSaturday.setEnabled(!checkBoxDaily.isChecked());
                checkBoxSunday.setEnabled(!checkBoxDaily.isChecked());
/*
                checkBoxMonday.setChecked(!checkBoxDaily.isChecked());
                checkBoxThuesday.setChecked(!checkBoxDaily.isChecked());
                checkBoxWednesday.setChecked(!checkBoxDaily.isChecked());
                checkBoxThursday.setChecked(!checkBoxDaily.isChecked());
                checkBoxFriday.setChecked(!checkBoxDaily.isChecked());
                checkBoxSaturday.setChecked(!checkBoxDaily.isChecked());
                checkBoxSunday.setChecked(!checkBoxDaily.isChecked());*/
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.g_map);
        mapFragment.getMapAsync(this);
/*
        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY);
        }

        mapView = findViewById(R.id.g_map);
        mapView.onCreate(mapViewBundle);
        mapView.getMapAsync(this);*/
    }

    private void updateMapView() {

        if (mMap == null) {
            return;
        }

        if (circle != null && marker != null) {
            circle.remove();
            marker.remove();
        }

        String radiusString = textViewRadius.getText().toString();
        Integer radius = null;
        if (!radiusString.equals("")) {
            radius = Integer.valueOf(radiusString);
        }
        String address = textViewAddress.getText().toString();

        if (radius != null && address != null) {
            position = PositionService.getAddressFromLocation(address, this);
            if (position != null) {
                marker = mMap.addMarker(new MarkerOptions().position(position).title(textViewAddress.getText().toString()));

                mMap.moveCamera(CameraUpdateFactory
                        .newLatLngZoom(position, 14.0f));

                CircleOptions circleOptions = new CircleOptions()
                        .center(position)
                        .radius(radius)
                        .strokeColor(Color.RED);
                circle = mMap.addCircle(circleOptions);
            }
        }
    }

    private void save() {
        PositionData data = new PositionData();
        data.setName(textViewName.getText().toString());
        data.setAddress(textViewAddress.getText().toString());
        data.setTimeTo(textViewTimeTo.getText().toString());
        data.setTimeFrom(textViewTimeFrom.getText().toString());
        data.setRadius(Integer.valueOf(textViewRadius.getText().toString()));

        data.setDaily(checkBoxDaily.isChecked());
        data.setMonday(checkBoxMonday.isChecked());
        data.setTuesday(checkBoxThuesday.isChecked());
        data.setWednesday(checkBoxWednesday.isChecked());
        data.setThursday(checkBoxThursday.isChecked());
        data.setFriday(checkBoxFriday.isChecked());
        data.setSaturday(checkBoxSaturday.isChecked());
        data.setSunday(checkBoxSunday.isChecked());

        data.setMode((int) spinnerMode.getSelectedItemId());

        if (position != null) {
            data.setLatitude((float) position.latitude);
            data.setLongtitude((float) position.longitude);
        }
        if (id == null) {
            databaseService.insertData(data);
        } else {
            delete();
            databaseService.insertData(data);
        }
    }

    private void delete() {
        databaseService.deleteData(id);
    }

    private void loadData(PositionData data) {
        textViewAddress.setText(data.getAddress());
        textViewName.setText(data.getName());
        textViewRadius.setText(String.valueOf(data.getRadius()));
        textViewTimeFrom.setText(data.getTimeFrom());
        textViewTimeTo.setText(data.getTimeTo());
        spinnerMode.setSelection(data.getMode());
        setDaily(data);

    }

    private void setDaily(PositionData data) {
        if (data.getDaily()) {
            checkBoxDaily.setChecked(data.getDaily());
            checkBoxMonday.setChecked(false);
            checkBoxThuesday.setChecked(false);
            checkBoxWednesday.setChecked(false);
            checkBoxThursday.setChecked(false);
            checkBoxFriday.setChecked(false);
            checkBoxSaturday.setChecked(false);
            checkBoxSunday.setChecked(false);

            checkBoxMonday.setEnabled(false);
            checkBoxThuesday.setEnabled(false);
            checkBoxWednesday.setEnabled(false);
            checkBoxThursday.setEnabled(false);
            checkBoxFriday.setEnabled(false);
            checkBoxSaturday.setEnabled(false);
            checkBoxSunday.setEnabled(false);
        } else {
            checkBoxDaily.setChecked(data.getDaily());
            checkBoxMonday.setChecked(data.getMonday());
            checkBoxThuesday.setChecked(data.getTuesday());
            checkBoxWednesday.setChecked(data.getWednesday());
            checkBoxThursday.setChecked(data.getThursday());
            checkBoxFriday.setChecked(data.getFriday());
            checkBoxSaturday.setChecked(data.getSaturday());
            checkBoxSunday.setChecked(data.getSunday());

            checkBoxDaily.setEnabled(true);
            checkBoxMonday.setEnabled(true);
            checkBoxThuesday.setEnabled(true);
            checkBoxWednesday.setEnabled(true);
            checkBoxThursday.setEnabled(true);
            checkBoxFriday.setEnabled(true);
            checkBoxSaturday.setEnabled(true);
            checkBoxSunday.setEnabled(true);
        }
    }

    /*
        @Override
        public void onSaveInstanceState(Bundle outState) {
            super.onSaveInstanceState(outState);

            Bundle mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY);
            if (mapViewBundle == null) {
                mapViewBundle = new Bundle();
                outState.putBundle(MAP_VIEW_BUNDLE_KEY, mapViewBundle);
            }

            mapView.onSaveInstanceState(mapViewBundle);
        }
    */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        updateMapView();
    }
}
