package vondracek.uhk.soundcontrol.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import vondracek.uhk.soundcontrol.CustomAdapter;
import vondracek.uhk.soundcontrol.data.PositionDataShow;
import vondracek.uhk.soundcontrol.R;
import vondracek.uhk.soundcontrol.service.DatabaseService;
import vondracek.uhk.soundcontrol.service.ControlService;
import vondracek.uhk.soundcontrol.service.PositionService;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private FloatingActionButton floatingActionButton;

    ArrayList<PositionDataShow> dataModels;
    private ListView listView;
    private static CustomAdapter adapter;
    private TextView textView;
    private Button button;

    private Switch notificationSwitch;
    private Switch enabledSwitch;

    private PositionService positionService;
    private Location location = null;

    DatabaseService databaseService = new DatabaseService(this);

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //databaseService.deleteData();

        if(databaseService.getDataNastaveni(2) == 1){
            Intent intent = new Intent(this, ControlService.class);
            startService(intent);
        }



        floatingActionButton = findViewById(R.id.floatingActionButton);
        button = findViewById(R.id.button);
        listView = findViewById(R.id.listView);
        textView = findViewById(R.id.textView);
        notificationSwitch = findViewById(R.id.switch2);
        enabledSwitch = findViewById(R.id.switch1);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent Intent = new Intent(getApplicationContext(), AddRuleActivity.class);
                startActivity(Intent);
            }
        });
/*
        positionService = new PositionService(this, new PositionService.PositionChangeListener() {
            @Override
            public void finished(Location loc) {
                location = loc;
                Toast.makeText(this, "position changed " + loc.getLongitude() + " " + loc.getLatitude(), Toast.LENGTH_SHORT).show();

            }
        });*/
        button.setVisibility(View.INVISIBLE);
        textView.setVisibility(View.INVISIBLE);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                textView.setText("hledám...");

                positionService.searchPosition();
            }
        });

        notificationSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(notificationSwitch.isChecked()){
                    databaseService.deleteDataNastaveni(1);
                    databaseService.insertDataNastaveni(1, 1);
                }else{
                    databaseService.deleteDataNastaveni(1);
                    databaseService.insertDataNastaveni(1, 0);
                }
            }
        });
        if(databaseService.getDataNastaveni(1) == 1){
            notificationSwitch.setChecked(true);
        }else{
            notificationSwitch.setChecked(false);
        }

        if(databaseService.getDataNastaveni(2) == 1){
            enabledSwitch.setChecked(true);
        }else{
            enabledSwitch.setChecked(false);
        }

        enabledSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(enabledSwitch.isChecked()){
                    databaseService.deleteDataNastaveni(2);
                    databaseService.insertDataNastaveni(2, 1);
                }else{
                    databaseService.deleteDataNastaveni(2);
                    databaseService.insertDataNastaveni(2, 0);
                }
            }
        });

    }

    private void loadListView() {

        dataModels = databaseService.readAll();

        adapter = new CustomAdapter(dataModels, getApplicationContext());

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent myIntent = new Intent(getApplicationContext(), AddRuleActivity.class);
                Bundle bundle = new Bundle();
                PositionDataShow item = adapter.getItem(position);
                bundle.putString("id", String.valueOf(item.getId()));
                myIntent.putExtras(bundle);
                startActivity(myIntent);

            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        loadListView();
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadListView();
    }
}
