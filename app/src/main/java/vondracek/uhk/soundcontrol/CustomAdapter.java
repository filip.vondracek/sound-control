package vondracek.uhk.soundcontrol;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import vondracek.uhk.soundcontrol.data.PositionDataShow;
import vondracek.uhk.soundcontrol.service.Utils;

public class CustomAdapter extends ArrayAdapter<PositionDataShow>{

    private ArrayList<PositionDataShow> dataSet;
    Context mContext;

    private static class ViewHolder {
        TextView txtName;
        TextView txtAddress;
        TextView txtdays;
    }

    public CustomAdapter(ArrayList<PositionDataShow> data, Context context) {
        super(context, R.layout.item, data);
        this.dataSet = data;
        this.mContext=context;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        PositionDataShow dataModel = getItem(position);
        ViewHolder viewHolder;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item, parent, false);
            viewHolder.txtName = convertView.findViewById(R.id.name);
            viewHolder.txtAddress = convertView.findViewById(R.id.address);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.txtName.setText(dataModel.getName());
        viewHolder.txtAddress.setText(dataModel.getAddress() + ", " + Utils.soundModeToString(dataModel.getMode()));
        return convertView;
    }
}

