package vondracek.uhk.soundcontrol;

import android.location.Location;

import org.junit.Test;

import vondracek.uhk.soundcontrol.service.DatabaseService;
import vondracek.uhk.soundcontrol.service.PositionService;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void test2(){
        boolean positionInCircle = PositionService.isPositionInCircle(49.971872, 16.391659,
                500, 49.972965, 16.391019);
        assertTrue(positionInCircle);
    }
}